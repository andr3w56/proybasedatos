﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmBuscarEstudiante : Form
    {
        public frmBuscarEstudiante()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtMatricula.Text, out int n);
            if (txtMatricula.Text.Length == 0 || esLetra(txtMatricula.Text) || n < 0)
            {
                MessageBox.Show("Debe ingresar un numero de matrícula válido");
                return;
            }
            else if (txtMatricula.Text.Length > 5 || txtMatricula.Text.Length < 5)
            {
                MessageBox.Show("El número de matricula deben ser (5) valores númericos.");
                return;
            }
            DataAccessObject.DAO oEst = new DataAccessObject.DAO();
            string matricula = this.txtMatricula.Text;
            DataTable dt = oEst.getEstudiante(matricula);
            //recorro los datos recuperados
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtApellidos.Text = fila["apellidos"].ToString();
                this.txtNombres.Text = fila["nombres"].ToString();
                this.txtEstatura.Text = fila["estatura"].ToString();
                this.txtFechaNacimiento.Text = Convert.ToDateTime(fila["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy");
                //tarea: mostrar solo 2 decimales
                float.TryParse(fila["peso"].ToString(), out float peso);
                this.txtPeso.Text = peso.ToString("0.00");
                this.txtCreacion.Text = fila["fechaCreacion"].ToString();
            }
            //tarea: muestre el mensaje adecuado, en caso que el estudiante no exista
            if (this.txtApellidos.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado al estudiante.");
            }
        } 
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }
        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
