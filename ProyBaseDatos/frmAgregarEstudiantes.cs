﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmAgregarEstudiantes : Form
    {
        public frmAgregarEstudiantes()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                // Tarea: Validar que se ingrese matricula, apellidos, etc.
                ProyBaseDatos.DataAccessObject.Estudiante est = new ProyBaseDatos.DataAccessObject.Estudiante();
                est.matricula = this.txtCodigoLibro.Text;
                est.apellidos = this.txtApellidos.Text;
                est.nombres = this.txtNombres.Text;
                est.estatura = Int32.Parse(this.txtEstatura.Text);
                est.fechaNacimiento = this.dtFechaNacimiento.Value;
                est.peso = float.Parse(this.txtPeso.Text) * ((float)1.0);

                ProyBaseDatos.DataAccessObject.DAO objEstudiante = new ProyBaseDatos.DataAccessObject.DAO();
                //Llamo al método para guardar el registro.
                int x = objEstudiante.guardar(est);
                if (x > 0)
                    MessageBox.Show("Estudiante agregado con éxito.");
                else
                    MessageBox.Show("No se pudo agregar el estudiante");

            }
            catch (FormatException ex)
            {
                MessageBox.Show("Faltan ingresar datos, o están en el formato incorrecto");
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtFechaNacimiento_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
