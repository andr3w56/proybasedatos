﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmActualizarEstudiantes : Form
    {
        public frmActualizarEstudiantes()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtMatricula.Text, out int n);
            if (txtMatricula.Text.Length == 0 || esLetra(txtMatricula.Text) || n < 0)
            {
                MessageBox.Show("Debe ingresar un numero de matrícula válido");
                return;
            }
            else if (txtMatricula.Text.Length > 5) 
            {
                MessageBox.Show("El número de matricula deben ser solo (5) valores númericos.");
                return;
            }
            DataAccessObject.DAO oEst = new DataAccessObject.DAO();
            string matricula = this.txtMatricula.Text;
            DataTable dt = oEst.getEstudiante(matricula);
            //recorro los datos recuperados
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtApellidos.Text = fila["apellidos"].ToString();
                this.txtNombres.Text = fila["nombres"].ToString();
                this.txtEstatura.Text = fila["estatura"].ToString();
                DateTime fecha = DataAccessObject.DAO.GetDateFromDatabase(matricula);
                this.dtFechaNacimiento.Value = fecha;
                //this.txtFechaNacimiento.Text = Convert.ToDateTime(fila["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy");
                //tarea: mostrar solo 2 decimales
                float.TryParse(fila["peso"].ToString(), out float peso);
                this.txtPeso.Text = peso.ToString("0.00");
            }
            //tarea: muestre el mensaje adecuado, en caso que el estudiante no exista
            if (this.txtApellidos.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado al estudiante.");
            }
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        { 
            if (Int32.TryParse(txtMatricula.Text, out int matriculanum))
            {
                //tarea: Primero debe mostrar el mensaje: Está seguro que desea eliminar este registro. Si el
                //usuario seleccione SI, se procede a eliminar el registro.
                DialogResult confirmarActualizacion = MessageBox.Show("¿Está seguro que desea actualizar este estudiante?", "Confirmar eliminación", MessageBoxButtons.YesNo);
                if (confirmarActualizacion == DialogResult.Yes)
                {
                    try
                    {
                        ProyBaseDatos.DataAccessObject.Estudiante est = new ProyBaseDatos.DataAccessObject.Estudiante
                        {
                            matricula = this.txtMatricula.Text,
                            apellidos = this.txtApellidos.Text,
                            nombres = this.txtNombres.Text,
                            estatura = Int32.Parse(this.txtEstatura.Text),
                            fechaNacimiento = this.dtFechaNacimiento.Value,
                            peso = float.Parse(this.txtPeso.Text) * ((float)1.0)
                        };
                        ProyBaseDatos.DataAccessObject.DAO objEstudiante = new ProyBaseDatos.DataAccessObject.DAO();

                        DataAccessObject.DAO oEst = new DataAccessObject.DAO();
                        int x = objEstudiante.actualizar(est);
                        if (x > 0)
                        {
                            MessageBox.Show("Registro actualizado con éxito.");
                            //tarea: limpiar los cuadros de texto
                            limpiarTxts(groupBox1);
                            dtFechaNacimiento.ResetText();
                        }
                        else
                            MessageBox.Show("No se pudo actualizar el registro.");

                    }
                    catch (FormatException)
                    { 
                        MessageBox.Show("Asegurate de que estén todos los datos ingresados y en el formato correcto.");
                    }
                }
                else
                    return;
            }
            else
            {
                MessageBox.Show("Debe ingresar un número de matricula.");
                return;
            }
        }
        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
