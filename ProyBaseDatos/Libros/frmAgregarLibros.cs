﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos.Libros
{
    public partial class frmAgregarLibros : Form
    {
        public frmAgregarLibros()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        { 
            try
            {
                ProyBaseDatos.Libros.DAOLibros.Libro lib = new ProyBaseDatos.Libros.DAOLibros.Libro();
                lib.codigo = this.txtCodigoLibro.Text;
                lib.nombre = this.txtNombreLibro.Text;
                lib.autor = this.txtAutor.Text;
                lib.precio = float.Parse(this.txtPrecioCompra.Text) * ((float)1.00);
                lib.fechaCompra = this.dtFechaCompra.Value;
                lib.unidades = Int32.Parse(this.txtUnidades.Text);

                DAOLibros.DAOL dAOL = new DAOLibros.DAOL();
                //Llamo al método para guardar el registro.
                int x = dAOL.guardar(lib);
                if (x > 0)
                    MessageBox.Show("Libro agregado con éxito.");
                else
                    MessageBox.Show("No se pudo agregar el Libro");

            }
            catch (FormatException ex)
            {
                MessageBox.Show("Faltan ingresar datos, o están en el formato incorrecto");
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
