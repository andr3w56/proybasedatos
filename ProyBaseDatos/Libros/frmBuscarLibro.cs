﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmBuscarLibro : Form
    {
        public frmBuscarLibro()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtCodigoLibro.Text, out int n);
            if (txtCodigoLibro.Text.Length == 0 || esLetra(txtCodigoLibro.Text) || n < 0)
           {
               MessageBox.Show("Debe ingresar un codigo válido (solo números)");
               return;
           }
           else if (txtCodigoLibro.Text.Length > 10) 
           {
               MessageBox.Show("El código debe ser solo (10) valores númericos.");
               return;
           }
            Libros.DAOLibros.DAOL dAOL = new Libros.DAOLibros.DAOL(); 
            string codigo = this.txtCodigoLibro.Text;
            DataTable dt = dAOL.getLibro(codigo);
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtNombreLibro.Text = fila["nombreLibro"].ToString();
                this.txtAutor.Text = fila["autor"].ToString();
                float.TryParse(fila["precioCompra"].ToString(), out float precio);
                this.txtPrecioCompra.Text = precio.ToString("0.00");
                this.txtFechaCompra.Text = Convert.ToDateTime(fila["fechaCompra"].ToString()).ToString("dd/MM/yyyy");
                this.txtUnidades.Text = fila["unidades"].ToString();
                this.txtCreacion.Text = fila["fechaCreacion"].ToString();
            }
            if (this.txtNombreLibro.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado el Libro.");
            }
        } 
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }
        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
