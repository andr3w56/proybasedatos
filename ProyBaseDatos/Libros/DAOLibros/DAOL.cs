﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyBaseDatos.Libros.DAOLibros
{
    public class DAOL
    { 
        private static string cadenaConexion = @"Server=132.145.16.235,1433; database=ANDREWTICS2022; user id=sa; pwd=Ferandrew_049787561";
        public int guardar(Libro libro)
        { 
            //Creo la conexión con el motor de base de datos.
            SqlConnection conexion = new SqlConnection(cadenaConexion);

            //Creo el comando que guarda los registros en la BDD.
            string sql = "insert into Libros(codLibro, nombreLibro, autor, precioCompra, fechaCompra, unidades) " +
                "values(@codLibro, @nombreLibro, @autor, @precioCompra, @fechaCompra, @unidades)";
            SqlCommand comando = new SqlCommand(sql, conexion);

            //abro la conexion
            conexion.Open();

            comando.Parameters.Add(new SqlParameter("@codLibro", libro.codigo));
            comando.Parameters.Add(new SqlParameter("@nombreLibro", libro.nombre));
            comando.Parameters.Add(new SqlParameter("@autor", libro.autor));
            comando.Parameters.Add(new SqlParameter("@precioCompra", libro.precio));
            comando.Parameters.Add(new SqlParameter("@fechaCompra", libro.fechaCompra));
            comando.Parameters.Add(new SqlParameter("@unidades", libro.unidades));

            int resultado = comando.ExecuteNonQuery();

            conexion.Close();
            return resultado;

        }
        public DataTable getLibro(string codigo)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que busca el registro
            string sql = "SELECT nombreLibro, autor, precioCompra, fechaCompra, unidades, fechaCreacion " +
            "from Libros where codLibro=@codLibro";
            //declaro un objeto tipo datatable
            DataTable dt = new DataTable();
            //declaro un adaptador de datos
            SqlDataAdapter ad = new SqlDataAdapter(sql, conexion);
            //agrego el parámetro codigo
            ad.SelectCommand.Parameters.Add(new SqlParameter("@codLibro", codigo));
            //lleno el datatable dt
            ad.Fill(dt);
            //retorno el datatable dt
            return dt;
        }
        public int eliminar(string codigo)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que elimina los registros en la BDD
            string sql = "delete from Libros where codLibro=@codLibro";
            SqlCommand comando = new SqlCommand(sql, conexion);
            //abro la conexión
            conexion.Open();
            //agrego los parámetros
            comando.Parameters.Add(new SqlParameter("@codLibro", codigo));
            //ejecuto el comando (elimina el registro en la BDD)
            int resultado = comando.ExecuteNonQuery();
            //cerrar la conexión
            conexion.Close();
            return resultado;
        }
        public int actualizar(Libro libro)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que ACTUALIZA los registros en la BDD
            string sql = "UPDATE Libros SET nombreLibro=@nombreLibro, autor=@autor, precioCompra=@precioCompra, fechaCompra=@fechaCompra, unidades=@unidades WHERE codLibro=@codLibro";


            SqlCommand comando = new SqlCommand(sql, conexion);
            //abro la conexión
            conexion.Open();
            //agrego los parámetros
            comando.Parameters.Add(new SqlParameter("@nombreLibro", libro.nombre));
            comando.Parameters.Add(new SqlParameter("@autor", libro.autor));
            comando.Parameters.Add(new SqlParameter("@precioCompra", libro.precio));
            comando.Parameters.Add(new SqlParameter("@fechaCompra", libro.fechaCompra));
            comando.Parameters.Add(new SqlParameter("@unidades", libro.unidades));
            comando.Parameters.Add(new SqlParameter("@codLibro", libro.codigo));
            //ejecuto el comando (actualiza el registro en la BDD)
            int resultado = comando.ExecuteNonQuery();
            //cerrar la conexión
            conexion.Close();
            return resultado;
        }
        public static DateTime GetDateFromDatabase(string codigo)
        {
            string query = "SELECT fechaCompra FROM Libros WHERE codLibro = @codLibro";

            using (SqlConnection connection = new SqlConnection(cadenaConexion))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@codLibro", codigo);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            DateTime fecha = reader.GetDateTime(reader.GetOrdinal("fechaCompra"));
                            return fecha;
                        }
                        else
                        {
                            return DateTime.MinValue;
                        }
                    }
                }
            }
        }
    }
}
