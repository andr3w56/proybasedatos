﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyBaseDatos.Libros.DAOLibros
{
    public class Libro
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string autor { get; set; }
        public float precio { get; set; }
        public DateTime fechaCompra { get; set; }
        public int unidades { get; set; }
    }
}
