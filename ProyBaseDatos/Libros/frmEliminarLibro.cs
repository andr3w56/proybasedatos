﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmEliminarLibro : Form
    {
        public frmEliminarLibro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtCodigoLibro.Text, out int n);
            if (txtCodigoLibro.Text.Length == 0 || esLetra(txtCodigoLibro.Text) || n < 0)
            {
               MessageBox.Show("Debe ingresar un codigo válido (solo números)");
                return;
            }
            else if (txtCodigoLibro.Text.Length > 10)
            {
               MessageBox.Show("El código debe ser solo (10) valores númericos.");
                return;
            }
            Libros.DAOLibros.DAOL dAOL = new Libros.DAOLibros.DAOL(); 
            string codigo = this.txtCodigoLibro.Text;
            DataTable dt = dAOL.getLibro(codigo);
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtNombreLibro.Text = fila["nombreLibro"].ToString();
                this.txtAutor.Text = fila["autor"].ToString();
                float.TryParse(fila["precioCompra"].ToString(), out float precio);
                this.txtPrecioCompra.Text = precio.ToString("0.00");
                this.txtFechaCompra.Text = Convert.ToDateTime(fila["fechaCompra"].ToString()).ToString("dd/MM/yyyy");
                this.txtUnidades.Text = fila["unidades"].ToString();
                this.txtCreacion.Text = fila["fechaCreacion"].ToString();
            }
            if (this.txtNombreLibro.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado el Libro.");
            }
        } 
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(txtCodigoLibro.Text, out int codigonum))
            {
                DialogResult confirmarEliminacion = MessageBox.Show("¿Está seguro que desea eliminar este libro?", "Confirmar eliminación", MessageBoxButtons.YesNo);
                if (confirmarEliminacion == DialogResult.Yes)
                {
                    string codigo = this.txtCodigoLibro.Text;
                    Libros.DAOLibros.DAOL dAOL = new Libros.DAOLibros.DAOL();
                    int x = dAOL.eliminar(codigo);
                    if (x > 0)
                    {
                        MessageBox.Show("Registro eliminado con éxito.");
                        limpiarTxts(groupBox1);
                    }
                    else
                        MessageBox.Show("No se pudo eliminar el registro.");
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un codigo válido (solo números).");
                return;
            }
        }

        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }
    }
}
