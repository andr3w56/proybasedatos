﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmActualizarLibro : Form
    {
        public frmActualizarLibro()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtCodigoLibro.Text, out int n);
            if (txtCodigoLibro.Text.Length == 0 || esLetra(txtCodigoLibro.Text) || n < 0)
            {
               MessageBox.Show("Debe ingresar un codigo válido (solo números)");
                return;
            }
           else if (txtCodigoLibro.Text.Length > 10) 
            {
               MessageBox.Show("El código debe ser solo (10) valores númericos.");
                return;
            }
            Libros.DAOLibros.DAOL dAOL = new Libros.DAOLibros.DAOL(); 
            string codigo = this.txtCodigoLibro.Text;
            DataTable dt = dAOL.getLibro(codigo);
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtNombreLibro.Text = fila["nombreLibro"].ToString();
                this.txtAutor.Text = fila["autor"].ToString();
                float.TryParse(fila["precioCompra"].ToString(), out float precio);
                this.txtPrecioCompra.Text = precio.ToString("0.00");
                DateTime fecha = Libros.DAOLibros.DAOL.GetDateFromDatabase(codigo); 
                this.dtFechaCompra.Value = fecha;
                this.txtUnidades.Text = fila["unidades"].ToString();
            }
            if (this.txtNombreLibro.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado el Libro.");
            }
        }
        private void btnActualizar_Click(object sender, EventArgs e)
        { 
            if (Int32.TryParse(txtCodigoLibro.Text, out int matriculanum))
            {
                DialogResult confirmarActualizacion = MessageBox.Show("¿Está seguro que desea actualizar este libro?", "Confirmar eliminación", MessageBoxButtons.YesNo);
                if (confirmarActualizacion == DialogResult.Yes)
                {
                    try
                    {
                        Libros.DAOLibros.Libro lib = new Libros.DAOLibros.Libro()
                        {
                            codigo = this.txtCodigoLibro.Text,
                            nombre = this.txtNombreLibro.Text,
                            autor = this.txtAutor.Text,
                            precio = float.Parse(this.txtPrecioCompra.Text) * ((float)1.0),
                            fechaCompra = this.dtFechaCompra.Value,
                            unidades = Int32.Parse(this.txtUnidades.Text)
                        };
                        Libros.DAOLibros.DAOL objLibro = new Libros.DAOLibros.DAOL();

                        int x = objLibro.actualizar(lib);
                        if (x > 0)
                        {
                            MessageBox.Show("Registro actualizado con éxito.");
                            //tarea: limpiar los cuadros de texto
                            limpiarTxts(groupBox1);
                            dtFechaCompra.ResetText();
                        }
                        else
                            MessageBox.Show("No se pudo actualizar el registro.");
                    }
                    catch (FormatException)
                    { 
                        MessageBox.Show("Asegurate de que estén todos los datos ingresados y en el formato correcto.");
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar codigo válido.");
                return;
            }
        }
        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
