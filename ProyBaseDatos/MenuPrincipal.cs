﻿using ProyBaseDatos.Libros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class MenuPrincipal : Form
    {
        private int childFormNumber = 0;

        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            frmAgregarEstudiantes Agregar = new frmAgregarEstudiantes();
            Agregar.MdiParent = this;
            Agregar.WindowState = FormWindowState.Maximized;
            Agregar.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        { 
            frmBuscarEstudiante Buscar = new frmBuscarEstudiante();
            Buscar.MdiParent = this;
            Buscar.WindowState = FormWindowState.Maximized;
            Buscar.Show();
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmActualizarEstudiantes Actualizar = new frmActualizarEstudiantes();
            Actualizar.MdiParent = this;
            Actualizar.WindowState = FormWindowState.Maximized;
            Actualizar.Show();
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void toolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void fileMenu_Click(object sender, EventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            frmEliminarEstudiante Eliminar = new frmEliminarEstudiante();
            Eliminar.MdiParent = this;
            Eliminar.WindowState = FormWindowState.Maximized;
            Eliminar.Show();
        }

        private void agregarNuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAgregarLibros AggLibro = new frmAgregarLibros();
            AggLibro.MdiParent = this;
            AggLibro.WindowState = FormWindowState.Maximized;
            AggLibro.Show();
        }

        private void administrarLibrosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBuscarLibro BuscarL = new frmBuscarLibro();
            BuscarL.MdiParent = this;
            BuscarL.WindowState = FormWindowState.Maximized;
            BuscarL.Show();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEliminarLibro EliminarL = new frmEliminarLibro();
            EliminarL.MdiParent = this;
            EliminarL.WindowState = FormWindowState.Maximized;
            EliminarL.Show();
        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmActualizarLibro ActualizarL = new frmActualizarLibro();
            ActualizarL.MdiParent = this;
            ActualizarL.WindowState = FormWindowState.Maximized;
            ActualizarL.Show();
        }
    }
}
