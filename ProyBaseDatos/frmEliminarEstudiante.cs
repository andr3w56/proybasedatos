﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyBaseDatos
{
    public partial class frmEliminarEstudiante : Form
    {
        public frmEliminarEstudiante()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Int32.TryParse(txtMatricula.Text, out int n);
            if (txtMatricula.Text.Length == 0 || esLetra(txtMatricula.Text) || n < 0)
            {
                MessageBox.Show("Debe ingresar un numero de matrícula válido");
                return;
            }
            else if (txtMatricula.Text.Length > 5 || txtMatricula.Text.Length < 5)
            {
                MessageBox.Show("El número de matricula deben ser (5) valores númericos.");
                return;
            }
            DataAccessObject.DAO oEst = new DataAccessObject.DAO();
            string matricula = this.txtMatricula.Text;
            DataTable dt = oEst.getEstudiante(matricula);
            //recorro los datos recuperados
            limpiarTxts(groupBox1);
            foreach (DataRow fila in dt.Rows)
            {
                this.txtApellidos.Text = fila["apellidos"].ToString();
                this.txtNombres.Text = fila["nombres"].ToString();
                this.txtEstatura.Text = fila["estatura"].ToString();
                this.txtFechaNacimiento.Text = Convert.ToDateTime(fila["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy");
                //tarea: mostrar solo 2 decimales
                float.TryParse(fila["peso"].ToString(), out float peso);
                this.txtPeso.Text = peso.ToString("0.00");
                this.txtCreacion.Text = fila["fechaCreacion"].ToString();
            }
            //tarea: muestre el mensaje adecuado, en caso que el estudiante no exista
            if (this.txtApellidos.Text.Length == 0)
            {
                MessageBox.Show("No se ha encontrado al estudiante.");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            //tarea: valide que se haya ingresado el número de matrícula
            if (Int32.TryParse(txtMatricula.Text, out int matriculanum))
            {
                //tarea: Primero debe mostrar el mensaje: Está seguro que desea eliminar este registro. Si el
                //usuario seleccione SI, se procede a eliminar el registro.
                DialogResult confirmarEliminacion = MessageBox.Show("¿Está seguro que desea eliminar este estudiante?", "Confirmar eliminación", MessageBoxButtons.YesNo);
                if (confirmarEliminacion == DialogResult.Yes)
                {
                    string matricula = this.txtMatricula.Text;
                    DataAccessObject.DAO oEst = new DataAccessObject.DAO();
                    int x = oEst.eliminar(matricula);
                    if (x > 0)
                    {
                        MessageBox.Show("Registro eliminado con éxito.");
                        //tarea: limpiar los cuadros de texto
                        limpiarTxts(groupBox1);
                    }
                    else
                        MessageBox.Show("No se pudo eliminar el registro.");
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Debe ingresar un número de matricula.");
                return;
            }
            //Ayúdese buscando 'cuadros o ventanas de diálogo de confirmación'
        }

        public bool esLetra(string txt)
        {
            foreach (char c in txt)
            {
                if (!Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
        private void limpiarTxts(GroupBox g)
        {
            foreach (Control i in g.Controls)
            {
                if (i is TextBox)
                {
                    i.Text = "";
                }
            }
        }
    }
}
