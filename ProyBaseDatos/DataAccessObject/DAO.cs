﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyBaseDatos.DataAccessObject
{
    public class DAO
    {
        private static string cadenaConexion = @"Server=132.145.16.235,1433; database=ANDREWTICS2022; user id=sa; pwd=Ferandrew_049787561";
        //Autenticación Windows --sin password
        //private string cadenaConexion = @"Server=L-EBC-004\MSSQLSERVER01; database=Andrew; integrated security=true";
        public int guardar(Estudiante estudiante)
        {
            //Creo la conexión con el motor de base de datos.
            SqlConnection conexion = new SqlConnection(cadenaConexion);

            //Creo el comando que guarda los registros en la BDD.
            string sql = "insert into Estudiantes(matricula, apellidos, nombres, estatura, fechaNacimiento, peso) " +
                "values(@matricula, @apellidos, @nombres, @estatura, @fechaNacimiento, @peso)";
            SqlCommand comando = new SqlCommand(sql, conexion);

            //abro la conexion
            conexion.Open();

            //agrego los parámetros
            comando.Parameters.Add(new SqlParameter("@matricula", estudiante.matricula));
            comando.Parameters.Add(new SqlParameter("@apellidos", estudiante.apellidos));
            comando.Parameters.Add(new SqlParameter("@nombres", estudiante.nombres));
            comando.Parameters.Add(new SqlParameter("@estatura", estudiante.estatura));
            comando.Parameters.Add(new SqlParameter("@fechaNacimiento", estudiante.fechaNacimiento));
            comando.Parameters.Add(new SqlParameter("@peso", estudiante.peso));


            //ejecuto el comando. (Guardar el registro en la BDD).
            int resultado = comando.ExecuteNonQuery();

            //cerrar la conexion
            conexion.Close();
            return resultado;
        }
        public DataTable getEstudiante(string matricula)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que busca el registro
            string sql = "select matricula, apellidos, nombres, estatura, fechaNacimiento, peso, fechaCreacion " +
            "from Estudiantes where matricula=@matricula";
            //declaro un objeto tipo datatable
            DataTable dt = new DataTable();
            //declaro un adaptador de datos
            SqlDataAdapter ad = new SqlDataAdapter(sql, conexion);
            //agrego el parámetro matricula
            ad.SelectCommand.Parameters.Add(new SqlParameter("@matricula", matricula));
            //lleno el datatable dt
            ad.Fill(dt);
            //retorno el datatable dt
            return dt;
        }
        public int eliminar(string matricula)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que elimina los registros en la BDD
            string sql = "delete from estudiantes where matricula=@matricula";
            SqlCommand comando = new SqlCommand(sql, conexion);
            //abro la conexión
            conexion.Open();
            //agrego los parámetros
            comando.Parameters.Add(new SqlParameter("@matricula", matricula));
            //ejecuto el comando (elimina el registro en la BDD)
            int resultado = comando.ExecuteNonQuery();
            //cerrar la conexión
            conexion.Close();
            return resultado;
        }
        public int actualizar(Estudiante estudiante)
        {
            //Creo la conexión con el motor de base de datos
            SqlConnection conexion = new SqlConnection(cadenaConexion);
            //Creo el comando que ACTUALIZA los registros en la BDD
            string sql = "UPDATE Estudiantes SET apellidos=@apellidos, nombres=@nombres, estatura=@estatura, fechaNacimiento=@fechaNacimiento, peso=@peso WHERE matricula=@matricula";


            SqlCommand comando = new SqlCommand(sql, conexion);
            //abro la conexión
            conexion.Open();
            //agrego los parámetros
            comando.Parameters.Add(new SqlParameter("@apellidos", estudiante.apellidos));
            comando.Parameters.Add(new SqlParameter("@nombres", estudiante.nombres));
            comando.Parameters.Add(new SqlParameter("@estatura", estudiante.estatura));
            comando.Parameters.Add(new SqlParameter("@fechaNacimiento", estudiante.fechaNacimiento));
            comando.Parameters.Add(new SqlParameter("@peso", estudiante.peso));
            comando.Parameters.Add(new SqlParameter("@matricula", estudiante.matricula));
            //ejecuto el comando (actualiza el registro en la BDD)
            int resultado = comando.ExecuteNonQuery();
            //cerrar la conexión
            conexion.Close();
            return resultado;
        }
        public static DateTime GetDateFromDatabase(string matricula)
        {
            string query = "SELECT fechaNacimiento FROM Estudiantes WHERE matricula = @matricula";

            using (SqlConnection connection = new SqlConnection(cadenaConexion))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@matricula", matricula);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            DateTime fecha = reader.GetDateTime(reader.GetOrdinal("fechaNacimiento"));
                            return fecha;
                        }
                        else
                        {
                            return DateTime.MinValue;
                        }
                    }
                }
            }
        }
    }
}
