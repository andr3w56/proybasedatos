﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyBaseDatos.DataAccessObject
{
    public class Estudiante
    {
        public string matricula { get; set; }
        public string apellidos { get; set; }
        public string nombres { get; set; }
        public int estatura { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public float peso { get; set; }
    }
}
